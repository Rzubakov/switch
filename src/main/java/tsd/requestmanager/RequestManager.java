/*
 * Copyright 2018 Rohit Awate.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tsd.requestmanager;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import tsd.Main;
import tsd.filters.CookieFilter;
import tsd.models.requests.DataRequest;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RequestManager extends Service<Response> {
    private Client client;

    public RequestManager() {
        client = ClientBuilder.newClient();
    }

    private DataRequest request;
    private WebTarget webTarget;

    /**
     * Creates a JavaFX Task for processing the required kind of request.
     */
    @Override
    protected Task<Response> createTask() throws ProcessingException {
        return new Task<Response>() {
            @Override
            protected Response call() throws Exception {
                webTarget = client.target(request.getTarget().toURI());
                webTarget.register(new CookieFilter("mgs", Main.getCookie()));
                Invocation.Builder invocation = webTarget.request(MediaType.APPLICATION_JSON_TYPE);
                return invocation.buildPost(Entity.entity(request.getBody(), request.getContentType())).invoke();
            }
        };
    }

    public void setRequest(DataRequest request) {
        this.request = request;
    }

    public void addHandlers(EventHandler<WorkerStateEvent> running,
                            EventHandler<WorkerStateEvent> succeeded,
                            EventHandler<WorkerStateEvent> failed,
                            EventHandler<WorkerStateEvent> cancelled) {
        setOnRunning(running);
        setOnSucceeded(succeeded);
        setOnFailed(failed);
        setOnCancelled(cancelled);
    }

    public void removeHandlers() {
        removeEventHandler(WorkerStateEvent.WORKER_STATE_RUNNING, getOnRunning());
        removeEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, getOnSucceeded());
        removeEventHandler(WorkerStateEvent.WORKER_STATE_FAILED, getOnFailed());
        removeEventHandler(WorkerStateEvent.WORKER_STATE_CANCELLED, getOnCancelled());
    }
}
