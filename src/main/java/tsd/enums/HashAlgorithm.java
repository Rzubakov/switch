package tsd.enums;

public enum HashAlgorithm {
    mac_da,
    mac_dasa,
    mac_dasa_vlan,
    mac_dasa_vlan_ip
}
