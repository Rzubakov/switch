package tsd.enums;

public enum ApiUrl {

    LOGIN("/api/session/login"),
    SESSION_LOGUT("/api/session/logout"),

    SYSTEM_PASSWORD_SET("/api/system/password/set"),
    SYSTEM_PARAMS_GET("/api/system/params/get"),
    SYSTEM_PARAMS_SET("/api/system/params/set"),

    L2_TABLE_GET("/api/l2/table/get"),
    L2_TABLE_COUNT_GET("/api/l2/table/count/get"),

    INTF_LIST_GET("/api/intf/list/get"),

    POST_LIST_GET("/api/port/list/get"),
    PORT_GET("/api/port/get"),
    PORT_SET("/api/port/set"),
    PORT_SPEED_MAX_GET("/api/port/speed/max/get"),
    PORT_PHYS_TYPE_GET("/api/port/phys/type/get"),
    PORT_LINK_STATE_GET("/api/port/link/state/get"),

    PORT_ADMIN_GET("/api/port/admin/get"),

    DOS_GET("/api/dos/get"),
    DOS_SET("/api/dos/set"),

    STORM_GET("/api/storm/get"),
    STORM_SET("/api/storm/set"),

    PORT_STATS_GET("/api/port/stats/get"),
    PORT_STATS_CLEAR("/api/port/stats/clear"),

    CABLE_GIAG_TEST("/api/cable/diag/test"),

    VLAN_GET("/api/vlan/get"),
    VLAN_SET("/api/vlan/set"),
    VLAN_MODE_GET("/api/vlan/mode/get"),
    VLAN_MODE_SET("/api/vlan/mode/set"),
    VLAN_GREATE("/api/vlan/create"),
    VLAN_DELETE("/api/vlan/delete"),

    MIRROR_GET("/api/mirror/get"),
    MIRROR_SET("/api/mirror/set"),
    MIRROR_DELETE("/api/mirror/delete"),

    LAG_GET("/api/lag/get"),
    LAG_PORTMAP_GET("/api/lag/portmap/get"),
    LAG_HASH_GET("/api/lag/hash/get"),
    LAG_CERATE("/api/lag/create"),
    LAG_DELETE("/api/lag/delete"),
    LAG_INTF_SET("/api/lag/intf/set"),
    LAG_HASH_SET("/api/lag/hash/set"),

    IGMPSNOOP_GLOBAL_GET("/api/igmpsnoop/global/get"),
    IGMPSNOOP_GLOBAL_SET("/api/igmpsnoop/global/set"),
    IGMPSNOOP_VLAN_GET("/api/igmpsnoop/vlan/get"),
    IGMPSNOOP_VLAN_SET("/api/igmpsnoop/vlan/set"),
    IGMPSNOOP_INTF_GET("/api/igmpsnoop/intf/get"),
    IGMPSNOOP_INTF_SET("/api/igmpsnoop/intf/set"),

    RSTP_GLOBAL_GET("/api/rstp/global/get"),
    RSTP_GLOBAL_SET("/api/rstp/global/set"),
    RSTP_INTF_GET("/api/rstp/intf/get"),
    RSTP_INTF_SET("/api/rstp/intf/set"),

    LLDP_TLV_GET("/api/lldp/tlv/get"),
    LLDP_TLV_SET("/api/lldp/tlv/set"),
    LLDP_TLV_MASK_GET("/api/lldp/tlv/mask/get"),
    LLDP_TLV_MASK_SET("/api/lldp/tlv/mask/set"),
    LLDP_INTF_GET("/api/lldp/intf/get"),
    LLDP_INTF_SET("/api/lldp/intf/set"),
    LLDP_STATS_GET("/api/lldp/stats/get"),
    LLDP_MIBINFO_GET("/api/lldp/mibinfo/get"),

    QOS_DOT1P_MAPPING_GET("/api/qos/dot1p/mapping/get"),
    QOS_DOT1P_MAPPING_SET("/api/qos/dot1p/mapping/set"),
    QOS_DSCP_GET("/api/qos/dscp/mapping/get"),
    QOS_DSCP_SET("/api/qos/dscp/mapping/set"),
    QOS_PORT_SCHED_GET("/api/qos/port/sched/get"),
    QOS_PORT_SCHED_SET("/api/qos/port/sched/set"),
    QOS_PORT_RATELIMIT_GET("/api/qos/port/ratelimit/get"),
    QOS_PORT_RATELIMIT_SET("/api/qos/port/ratelimit/set"),

    AUTOVOIP_OUI_CREATE("/api/autovoip/oui/create"),
    AUTOVOIP_OUI_DELETE("/api/autovoip/oui/delete"),
    AUTOVOIP_OUI_GET("/api/autovoip/oui/get"),
    AUTOVOIP_PORT_OUIS_GET("/api/autovoip/port/ouis/get"),
    AUTOVOIP_PORT_OUIS_SET("/api/autovoip/port/ouis/set"),

    CFG_VALIDATE("/api/cfg/validate"),
    CFG_SAVE("/api/cfg/save"),
    CFG_DEFAULT("/api/cfg/default"),
    DEV_CFG_VIEW("/api/dev/cfg/view"),

    FW_DATA_GET("/api/fw/data/get"),
    FW_VERSION_SET("/api/fw/version/set"),
    FW_IMAGE_TOGGLE("/api/fw/image/toggle");

    private String code;

    ApiUrl(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
