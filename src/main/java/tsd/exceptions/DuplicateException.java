package tsd.exceptions;

public class DuplicateException extends Exception {
    public DuplicateException(String message) {
        super(message);
    }
}
