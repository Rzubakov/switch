package tsd.filters;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.HttpHeaders;

public class CookieFilter implements ClientRequestFilter {
    private final String cookieName;
    private final String cookieValue;

    public CookieFilter(final String cookieName, final String cookieValue) {
        this.cookieName = cookieName;
        this.cookieValue = cookieValue;
    }

    @Override
    public void filter(ClientRequestContext clientRequestContext) {
        clientRequestContext.getHeaders().add(HttpHeaders.COOKIE, cookieName + "=" + cookieValue);
    }
}