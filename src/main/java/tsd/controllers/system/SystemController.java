package tsd.controllers.system;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SystemController implements Initializable {

    @FXML
    private Tab configurationTab;

    @FXML
    private Tab macTableTab;

    @FXML
    private Tab passwordTab;

    @FXML
    private Tab denialOfServiceTab;

    @FXML
    private Tab stormControlTab;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/system/configuration.fxml"));
            configurationTab.setContent(loader.load());

            loader = new FXMLLoader(getClass().getResource("/fxml/system/mac.fxml"));
            macTableTab.setContent(loader.load());

            loader = new FXMLLoader(getClass().getResource("/fxml/system/password.fxml"));
            passwordTab.setContent(loader.load());

            loader = new FXMLLoader(getClass().getResource("/fxml/system/denial.fxml"));
            denialOfServiceTab.setContent(loader.load());

            loader = new FXMLLoader(getClass().getResource("/fxml/system/stormControl.fxml"));
            stormControlTab.setContent(loader.load());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
