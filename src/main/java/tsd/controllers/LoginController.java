package tsd.controllers;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import tsd.enums.ApiUrl;
import tsd.models.requests.DataRequest;
import tsd.models.requests.HTTPConstants;
import tsd.requestmanager.RequestManager;
import tsd.requestmanager.RequestManagersPool;
import tsd.utils.JsonTools;
import tsd.Main;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="connect"
    private Button connectButton; // Value injected by FXMLLoader

    @FXML // fx:id="password"
    private PasswordField passwordField; // Value injected by FXMLLoader

    @FXML // fx:id="login"
    private TextField loginField; // Value injected by FXMLLoader

    @FXML // fx:id="ipAddress"
    private TextField ipAddressField; // Value injected by FXMLLoader

    @FXML
    private CheckBox sslEnableCheckBox;

    private RequestManager requestManager;

    private DataRequest dataRequest;

    private JsonTools jsonTools;

    private String address;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ipAddressField.setText("127.0.0.1");
        passwordField.setText("password");
        loginField.setText("password");
        init();
    }

    private void init() {
        jsonTools = new JsonTools();
        dataRequest = new DataRequest();
        connectButton.setOnAction(this::sendRequest);
    }

    private void changeView() {
        connectButton.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/leftMenu.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }

    public void sendRequest(Event event) {
        if (requestManager != null) {
            while (requestManager.isRunning()) {
                requestManager.cancel();
                requestManager.reset();
            }
        }
        try {
            if (ipAddressField.getText().trim().equals("")) {
                System.out.println("address empty");
                return;
            }
            address = sslEnableCheckBox.isSelected() ? "https://" : "http://" + ipAddressField.getText().trim() + ":8000";
            dataRequest.setTarget(address + ApiUrl.LOGIN.getCode());
            dataRequest.setContentType(MediaType.APPLICATION_JSON);
            dataRequest.setBody("{\"user\":" + "\"" + loginField.getText().trim() + "\"" + "," + "\"pwd\":" + "\"" + passwordField.getText().trim() + "\"" + "}");
            requestManager = RequestManagersPool.manager();
            requestManager.setRequest(dataRequest);
            requestManager.addHandlers(this::whileRunning, this::onSucceeded, this::onFailed, this::onCancelled);
            requestManager.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onFailed(Event event) {
        System.out.println("onFailed");
        requestManager.getException().printStackTrace();
        requestManager.reset();
    }

    private void onCancelled(Event event) {
        System.out.println("onCancelled");
        requestManager.reset();
    }

    private void onSucceeded(Event event) {
        // TODO delete string "Wrong password." from Moongose web server
        int status = jsonTools.getParamByNameAsInt(requestManager.getValue().readEntity(String.class).replace("Wrong password.", ""), "status");
        System.out.println(status);
        if (status == 0) {
            Main.setIpAddress(sslEnableCheckBox.isSelected() ? "https://" : "http://" + ipAddressField.getText().trim() + ":8000");
            Main.setCookie(requestManager.getValue().getHeaders().getFirst("Set-Cookie").toString().replace("mgs=",""));
            Main.setSslEnabled(sslEnableCheckBox.isSelected());
            changeView();
        }
        if (status == -4) {
            System.out.println("Wrong password.");
        }
        requestManager.reset();
    }

    private void whileRunning(Event event) {
        System.out.println("whileRunning login controller");
    }
}

