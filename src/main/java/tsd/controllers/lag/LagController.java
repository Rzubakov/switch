package tsd.controllers.lag;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import tsd.enums.HashAlgorithm;

import java.net.URL;
import java.util.ResourceBundle;

public class LagController implements Initializable {

    @FXML
    private ComboBox<String> hashAlgorithm;

    @FXML
    private ComboBox<String> lag;

    @FXML
    private TextField ifIndex;

    @FXML
    private TextField name;

    @FXML
    private ComboBox<?> membership;

    @FXML
    private TextField pvid;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    private void init() {

        hashAlgorithm.setItems(FXCollections.observableArrayList(
                HashAlgorithm.mac_da.toString(),
                HashAlgorithm.mac_dasa.toString(),
                HashAlgorithm.mac_dasa_vlan.toString(),
                HashAlgorithm.mac_dasa_vlan_ip.toString()));

        lag.setItems(FXCollections.observableArrayList(
                "new"
        ));

        hashAlgorithm.setOnAction(event -> {
            System.out.println(hashAlgorithm.getSelectionModel().getSelectedItem());
        });

        lag.setOnAction(event -> {
            System.out.println(hashAlgorithm.getSelectionModel().getSelectedItem());
        });
    }
}
