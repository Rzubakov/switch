package tsd.controllers;

import com.google.gson.JsonObject;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import tsd.Main;
import tsd.enums.ApiUrl;
import tsd.models.requests.DataRequest;
import tsd.requestmanager.RequestManager;
import tsd.requestmanager.RequestManagersPool;
import tsd.utils.JsonTools;

import javax.ws.rs.core.MediaType;
import java.net.URL;
import java.util.ResourceBundle;

public class HomeController implements Initializable {

    // Описание конфигурации
    @FXML
    private TextField configurationDescriptionField;

    // ID устройства
    @FXML
    private TextField deviceIdField;

    // Описание устройства
    @FXML
    private TextField deviceDescriptionField;

    // Локация устройства
    @FXML
    private TextField deviceLocationField;

    // Версия прошивки устройства
    @FXML
    private TextField firmwareVersionFiled;

    // Срок жизни L2 в секундах
    @FXML
    private TextField l2AgeTimeField;

    // Таймаут сессии в минутах
    @FXML
    private TextField sessionTimeoutField;

    // МАК адрес устройства
    @FXML
    private TextField macAddressField;

    // IPv4 адрес устройства
    @FXML
    private TextField ip4addressField;

    // Маска подсети
    @FXML
    private TextField subNetMaskField;

    // Шлюз по умолчанию
    @FXML
    private TextField gateWayField;

    // IPv6 адрес устройства
    @FXML
    private TextField ip6addressField;

    // DHCP
    @FXML
    private CheckBox dhcpCheckBox;

    // SSL
    @FXML
    private CheckBox sslCheckBox;

    // Кнопка сохранить конфигурацию
    @FXML
    private Button saveButton;

    // Кнопка обновить конфигурацию
    @FXML
    private Button refreshButton;

    private RequestManager requestManager;

    private DataRequest dataRequest;

    private JsonTools jsonTools;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dataRequest = new DataRequest();
        jsonTools = new JsonTools();
        sendRequest(null);
        saveButton.setOnAction(System.out::println);
        refreshButton.setOnAction(this::sendRequest);
    }

    public void sendRequest(Event event) {
        if (requestManager != null) {
            while (requestManager.isRunning()) {
                requestManager.cancel();
                requestManager.reset();
            }
        }
        try {
            dataRequest.setTarget(Main.getIpAddress() + ApiUrl.SYSTEM_PARAMS_GET.getCode());
            dataRequest.setContentType(MediaType.APPLICATION_JSON);
            dataRequest.addToHeaders("Cookie:", Main.getCookie());
            dataRequest.setBody("mgs=" + Main.getCookie());
            requestManager = RequestManagersPool.manager();
            requestManager.setRequest(dataRequest);
            requestManager.addHandlers(this::whileRunning, this::onSucceeded, this::onFailed, this::onCancelled);
            requestManager.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onFailed(Event event) {
        System.out.println(requestManager.getMessage());
        System.out.println(requestManager.getValue());
        requestManager.getException().printStackTrace();
        requestManager.reset();
    }

    private void onCancelled(Event event) {
        System.out.println("onCancelled");
        requestManager.reset();
    }

    private void onSucceeded(Event event) {
        JsonObject jsonObject = jsonTools.getJsonObject(requestManager.getValue().readEntity(String.class));
        int status = jsonObject.get("status").getAsInt();
        if (status == 0) {
            updateFields(jsonObject.get("params").getAsJsonObject());
        }
        requestManager.reset();
    }

    private void whileRunning(Event event) {
        System.out.println("whileRunning homeController");
    }

    private void updateFields(JsonObject jsonObject) {
        configurationDescriptionField.setText(jsonObject.get("cfg_descr").getAsString());
        deviceIdField.setText(jsonObject.get("dev_id").getAsString());
        deviceDescriptionField.setText(jsonObject.get("dev_id").getAsString());
        deviceLocationField.setText(jsonObject.get("dev_loc").getAsString());
        firmwareVersionFiled.setText(jsonObject.get("fw").getAsString());
        l2AgeTimeField.setText(jsonObject.get("l2Agetime").getAsString());
        sessionTimeoutField.setText(jsonObject.get("sessTTL").getAsString());
        macAddressField.setText(jsonObject.get("mac").getAsString());
        ip4addressField.setText(jsonObject.get("ipv4Cfg").getAsString());
        subNetMaskField.setText(jsonObject.get("subnetCfg").getAsString());
        gateWayField.setText(jsonObject.get("gwCfg").getAsString());
        ip6addressField.setText(jsonObject.get("ipv6").getAsString());
        dhcpCheckBox.setSelected(jsonObject.get("dhcpv4").getAsBoolean());
        sslCheckBox.setSelected(jsonObject.get("ssl").getAsBoolean());
    }
}
