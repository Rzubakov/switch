package tsd.controllers.port;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PortController implements Initializable {
    @FXML
    private Tab statusTab;

    @FXML
    private Tab configurationTab;

    @FXML
    private Tab statisticsTab;

    @FXML
    private Tab cableDiagnosticsTab;

    @FXML
    private Tab mirroringTab;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/system/configuration.fxml"));
            statusTab.setContent(loader.load());

            loader = new FXMLLoader(getClass().getResource("/fxml/system/mac.fxml"));
            configurationTab.setContent(loader.load());

            loader = new FXMLLoader(getClass().getResource("/fxml/system/password.fxml"));
            statisticsTab.setContent(loader.load());

            loader = new FXMLLoader(getClass().getResource("/fxml/system/denial.fxml"));
            cableDiagnosticsTab.setContent(loader.load());

            loader = new FXMLLoader(getClass().getResource("/fxml/system/stormControl.fxml"));
            mirroringTab.setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
