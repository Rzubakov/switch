package tsd;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.ws.rs.core.NewCookie;
import java.util.HashMap;
import java.util.Map;

public class Main extends Application {
    private static String ipAddress;
    private static String cookie;
    private static boolean sslEnabled;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/login.fxml"));
        primaryStage.setTitle("TeconSwitch");
        primaryStage.setScene(new Scene(root, 500, 500));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void setCookie(String cookie) {
        Main.cookie = cookie;
    }

    public static String getCookie() {
        return cookie;
    }

    public static String getIpAddress() {
        return ipAddress;
    }

    public static void setIpAddress(String ipAddress) {
        Main.ipAddress = ipAddress;
    }

    public static boolean isSslEnabled() {
        return sslEnabled;
    }

    public static void setSslEnabled(boolean sslEnabled) {
        Main.sslEnabled = sslEnabled;
    }
}
