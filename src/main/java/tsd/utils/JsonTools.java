package tsd.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class JsonTools {
    private Gson gson;
    private JsonObject jsonObject;
    public JsonTools(){
        gson = new Gson();
        jsonObject = new JsonObject();
    }
    public String getParamByNameAsString(String json, String paramName){
        jsonObject = gson.fromJson(json, JsonObject.class);
        return jsonObject.get(paramName).getAsString();
    }

    public int getParamByNameAsInt(String json, String paramName){
        jsonObject = gson.fromJson(json, JsonObject.class);
        return jsonObject.get(paramName).getAsInt();
    }

    public JsonObject getJsonObject(String json){
        jsonObject = gson.fromJson(json, JsonObject.class);
        return jsonObject;
    }

}
